package com.down.main;

import java.io.IOException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.down.utls.FileUtils;
import com.down.utls.LayoutUtils;
import com.down.utls.ThreadPool;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.keeley.core.DownFileFetch;
import com.keeley.core.DownFileInfoBean;
import com.keeley.listen.DownListener;
import com.xiaoleilu.hutool.util.DateUtil;
import com.xiaoleilu.hutool.util.ThreadUtil;

public class Addnewdownshowinfo extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text text;
	public Integer downidk = null;
	private Text info;
	String url;
	String logs;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public Addnewdownshowinfo(Shell parent, int style, String url, String logs) {
		super(parent, style);
		setText("SWT Dialog");
		this.url = url;
		this.logs = logs;
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		LayoutUtils.center(shell);
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.PRIMARY_MODAL);
		shell.setSize(450, 663);
		shell.setText("文件下载");
		shell.setLayout(new GridLayout(2, false));
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);

		Label label = new Label(shell, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label.setText("下载地址:");

		text = new Text(shell, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 5));
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);

		info = new Text(shell, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_info = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_info.heightHint = 489;
		info.setLayoutData(gd_info);
		text.setText(url==null?"":url);
		info.setText(logs==null?"":logs);
	}
}
